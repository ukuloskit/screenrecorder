//
//  RecordingSettings.h
//  screenrecorder
//
//  Created by Uku on 24/12/15.
//  Copyright © 2015 Uku. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecordingSettings : NSObject
    @property float width;
    @property float height;
    @property float left;
    @property float top;
@end
