//
//  main.m
//  screenrecorder
//
//  Created by Uku on 05/12/15.
//  Copyright © 2015 Uku. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Recorder.h"
#import "GCDWebServer.h"
#import "GCDWebServerDataResponse.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        NSUserDefaults *standardDefaults = [NSUserDefaults standardUserDefaults];
        
        NSInteger width  = [standardDefaults floatForKey:@"width"];
        NSInteger height = [standardDefaults integerForKey:@"height"];
        NSInteger top = [standardDefaults integerForKey:@"top"];
        NSInteger left = [standardDefaults integerForKey:@"left"];
        NSString *path = [standardDefaults stringForKey:@"path"];
        
        if (!width || !height || !top || !left || !path) {
            NSLog(@"Missing command line arguments");
            exit(1);
        }
        Recorder *recorder = [[Recorder alloc] init];
        
        recorder.outputPath = path;

        NSFileManager *fileManager = [NSFileManager defaultManager];

        [recorder startWithSettings:width withHeight:height withTop:top withLeft:left];
        
        while (![fileManager fileExistsAtPath:@"/tmp/stop"]) {
            NSLog(@"sleeping");
            [NSThread sleepForTimeInterval:1.0f];
        }
        [recorder stop];
//         [NSThread sleepForTimeInterval:10.0f];
        

        
    }
    return 0;
}
