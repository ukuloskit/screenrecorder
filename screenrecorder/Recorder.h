//
//  Recorder.m
//  screenrecorder
//
//  Created by Uku on 05/12/15.
//  Copyright © 2015 Uku. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface Recorder : NSObject <AVCaptureFileOutputDelegate, AVCaptureFileOutputRecordingDelegate>

@property (retain) AVCaptureSession* session;
@property (retain) AVCaptureScreenInput* input;
@property (retain) AVCaptureMovieFileOutput* output;
@property (retain) NSString* outputPath;

// File that the recording will be saved to
@property NSURL* file;

- (BOOL) startWithSettings:(NSInteger)width withHeight:(NSInteger)height withTop: (NSInteger)top withLeft:(NSInteger)left;
- (BOOL) stop;

#pragma mark AVCaptureFileOutputDelegate
- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection;

#pragma mark AVCaptureFileOutputRecordingDelegate
- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL fromConnections:(NSArray *)connections error:(NSError *)error;
- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didPauseRecordingToOutputFileAtURL:(NSURL *)fileURL fromConnections:(NSArray *)connections;
- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didResumeRecordingToOutputFileAtURL:(NSURL *)fileURL fromConnections:(NSArray *)connections;
- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didStartRecordingToOutputFileAtURL:(NSURL *)fileURL fromConnections:(NSArray *)connections;
- (void)captureOutput:(AVCaptureFileOutput *)captureOutput willFinishRecordingToOutputFileAtURL:(NSURL *)fileURL fromConnections:(NSArray *)connections error:(NSError *)error;

@end
