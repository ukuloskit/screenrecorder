//
//  Message.m
//  screenrecorder
//
//  Created by Uku on 22/12/15.
//  Copyright © 2015 Uku. All rights reserved.
//

@protocol Message

- (void)startRecording();
- (void)stopRecording();

@end