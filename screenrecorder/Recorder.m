
#import "Recorder.h"


@implementation Recorder

@synthesize session;
@synthesize input;
@synthesize output;
@synthesize file;

- (id) init {
    self.session = [[AVCaptureSession alloc] init];
    self.input   = [[AVCaptureScreenInput alloc] initWithDisplayID:CGMainDisplayID()];
    self.output  = [[AVCaptureMovieFileOutput alloc] init];
    [self.output setDelegate:self];
    return self;
}

- (BOOL) startWithSettings:(NSInteger)width withHeight:(NSInteger)height withTop: (NSInteger)top withLeft: (NSInteger)left {

    self.input.cropRect = CGRectMake(left, top, width, height);
    [self.session addInput:self.input];
    [self.session addOutput:self.output];
    self.file = [NSURL URLWithString:[@"file://" stringByAppendingString:self.outputPath]];
    [self.session startRunning];
    [self.output  startRecordingToOutputFileURL:self.file
                              recordingDelegate:self];
    
    return YES;
}


- (BOOL) stop {
    [self.session stopRunning];
    [self.output  stopRecording];
    return YES;
}


#pragma mark AVCaptureFileOutputDelegate

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
}


#pragma mark AVCaptureFileOutputRecordingDelegate

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL fromConnections:(NSArray *)connections error:(NSError *)error {
    NSLog(@"Did finish recording");
}

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didPauseRecordingToOutputFileAtURL:(NSURL *)fileURL fromConnections:(NSArray *)connections {
}

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didResumeRecordingToOutputFileAtURL:(NSURL *)fileURL fromConnections:(NSArray *)connections {
}

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didStartRecordingToOutputFileAtURL:(NSURL *)fileURL fromConnections:(NSArray *)connections {
}

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput willFinishRecordingToOutputFileAtURL:(NSURL *)fileURL fromConnections:(NSArray *)connections error:(NSError *)error {
    NSLog(@"will finish recording");
}

@end






















