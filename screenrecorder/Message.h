//
//  Message.h
//  screenrecorder
//
//  Created by Uku on 22/12/15.
//  Copyright © 2015 Uku. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Message

- (void)startRecording();
- (void)stopRecording();

@end