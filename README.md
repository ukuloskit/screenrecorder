# README #


### Getting the window position info ###


```
#!bash

#!/bin/bash
osascript -e 'tell app "System Events" to get size of window 1 of process "iOS Simulator"'
osascript -e 'tell app "System Events" to get position of window 1 of process "iOS Simulator"'
```

### Modifying OSX usability database ###
`tccutil.py`