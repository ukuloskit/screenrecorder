//
//  screenrecorderd.h
//  screenrecorderd
//
//  Created by Uku on 22/12/15.
//  Copyright © 2015 Uku. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "screenrecorderdProtocol.h"

// This object implements the protocol which we have defined. It provides the actual behavior for the service. It is 'exported' by the service to make it available to the process hosting the service over an NSXPCConnection.
@interface screenrecorderd : NSObject <screenrecorderdProtocol>
@end
