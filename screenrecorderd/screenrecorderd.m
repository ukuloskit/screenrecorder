//
//  screenrecorderd.m
//  screenrecorderd
//
//  Created by Uku on 22/12/15.
//  Copyright © 2015 Uku. All rights reserved.
//

#import "screenrecorderd.h"

@implementation screenrecorderd

// This implements the example protocol. Replace the body of this class with the implementation of this service's protocol.
- (void)upperCaseString:(NSString *)aString withReply:(void (^)(NSString *))reply {
    NSString *response = [aString uppercaseString];
    reply(response);
}

@end
